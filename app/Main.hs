module Main where

import Lib

import Text.Read (readMaybe)
import Control.Monad (unless)

main :: IO ()
main = do
  putStrLn "\nstarting...\n"
  putStrLn . unlines . map (\(idx, f) -> show idx ++ ") " ++ show f) $ implementedFunctorsWithIdx 
  putStrLn "0) Quit"
  line <- getLine 
  let select = readMaybe line :: Maybe Int 
      selectedFunctor = (\idx -> lookup (idx) implementedFunctorsWithIdx) =<< select
  unless (any (\a -> a == 0) select) $  do
    case selectedFunctor of
        Just IdentityFunctor -> identityREPL $ pure ()
        Just otherwise -> putStrLn $ "not yet implemented" ++ show otherwise
        Nothing -> putStrLn $ "No such option " ++ line
    main
  where
    allImplementedFunctors :: [ImplementedFunctors]
    allImplementedFunctors = enumFrom (toEnum 0)
    implementedFunctorsWithIdx = zip [1 ..] $ allImplementedFunctors
