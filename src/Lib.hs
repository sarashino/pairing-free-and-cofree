{-# LANGUAGE DeriveFunctor, DeriveFoldable, DeriveTraversable,
  TypeOperators, MultiParamTypeClasses, RankNTypes, GADTs #-}

module Lib
  ( 
ImplementedFunctors (..),
identityREPL
  ) where

import Control.Comonad
import Control.Comonad.Cofree
import Control.Monad.Co
import Control.Monad.Free
import Control.Natural
import Data.Functor
import Data.Functor.Coyoneda

data ImplementedFunctors = IdentityFunctor 
                         | StoreFunctor deriving (Show, Enum)

type Program t = Free (Coyoneda t)

program f a = Free $ Coyoneda f a

type Pairing f g = forall a b. f a -> Cofree g b -> (a, Cofree g b)

runPair ::
     (Functor g, Show a, Show b)
  => Pairing f g
  -> Program f a
  -> Cofree g b
  -> IO a
runPair evalPair action coaction = do
  let (a,b) = runAction action coaction
  putStrLn $ show a ++ " " ++ show b
  pure a
  where
    runAction (Pure a) (s :< _) = (a, s)
    runAction (Free (Coyoneda t f)) w =
      let (a, nextW) = evalPair f w
       in runAction (t a) nextW

-- Shift/Stream
data IdentityF a =
  Identity a
  deriving (Functor)

type Stream = Cofree IdentityF

cons :: a -> Stream a -> Stream a
cons a s = a :< Identity s

type Shift = Program IdentityF

shift :: a -> Shift a
shift s = program id (Identity $ Pure s)

evalStream :: Pairing IdentityF IdentityF
evalStream (Identity  a) (_ :< (Identity b)) = (a, b)

identityREPL :: Show a => Shift a -> IO a
identityREPL shiftActions = do
  putStrLn "type `shift` or `done`"
  line <- getLine
  case line of
        "shift" -> 
            let actions = shift =<< shiftActions in
                do
                  runPair evalStream actions stream
                  identityREPL actions
        -- done means same as Pure.
        -- in this case, `runPair` always return Pure at every cycle so that print current status.
        -- thus just return here.
        "done" -> do
              runPair evalStream shiftActions stream
  where
    stream :: Stream Int
    stream = coiter (\a -> Identity $ a + 1) 0


-- State/Store
data StoreF s a = Store (s -> a) s
  deriving (Functor)
data StateF s a where
  Get :: StateF s s
  Put :: s -> StateF s ()
type Store s = Cofree (StoreF s)
-- type Program t = Free (Coyoneda t)
type State s a = Program (StateF s) a
-- program f a = Free $ Coyoneda f a
-- type Program t = Free (Coyoneda (StateF s)) s
-- get :: State s a -> State s s
-- get s = program id $ Get $ s
-- put :: s -> State s ()
-- put x = program id (Put x (Pure ()))

evalStore :: Pairing (StateF s) (StoreF s)
evalStore Get (_ :< (Store t s)) = ( s,t s)
evalStore (Put x) s = ((), s)

-- a.k.a State/Moore
data MooreF i a =
  Moore a (i -> MooreF i a)
  deriving (Functor)

type Moore s = Cofree (MooreF s)
